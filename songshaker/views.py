import logging

from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, RedirectView, TemplateView
from django.views.generic.detail import SingleObjectMixin

from songshaker.models import Playlist
from songshaker.randomizers import TrackCombiner
from songshaker.spotify import SpotifyAuthenticator, SpotifyPlaylistCreator

log = logging.getLogger(__name__)


class LoginView(TemplateView):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        auth_url = SpotifyAuthenticator().get_auth_url()
        return super().get(request, auth_url=auth_url)


class AuthCallbackView(RedirectView):
    def get(self, request, *args, **kwargs):
        user = authenticate(request, code=request.GET['code'])
        if user:
            log.info(f'Logged in as user {user.username}')
            login(request, user)
            return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('playlist-random')


class LoginRequiredView(LoginRequiredMixin):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'


class PlayListDetailView(LoginRequiredView, DetailView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['access_token'] = self.request.user.token_set.last().access_token
        return context

    def get_queryset(self):
        return Playlist.objects.filter(user=self.request.user).prefetch_related('playlistentry_set')


class SubmitPlaylistView(LoginRequiredView, SingleObjectMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        object = self.get_object()
        if object.submitted_to_spotify is not None:
            log.info(
                f'Playlist {object.submitted_to_spotify} was already '
                f'submitted to Spotify {object.submitted_to_spotify}'
            )
            return

        uris = list(object.playlistentry_set.values_list('uri', flat=True))
        playlist_data = SpotifyPlaylistCreator(request.user.token_set.first()).create_spotify_playlist(
            username=self.request.user.username, name=object.name, track_uris=uris
        )

        object.submitted_to_spotify = timezone.now()
        object.spotify_uri = playlist_data.uri
        object.save()

        return super().get(request, playlist_id=object.pk, *args, **kwargs)

    def get_queryset(self):
        return Playlist.objects.filter(user=self.request.user)

    def get_redirect_url(self, playlist_id, *args, **kwargs):
        return reverse('playlist-detail', args=[playlist_id])


class RandomPlaylistView(LoginRequiredView, RedirectView):
    @transaction.atomic()
    def get(self, request, *args, **kwargs):
        tracks = TrackCombiner(self.request.user.token_set.last()).get_tracks()
        playlist_name = f'Playlist from {timezone.now().strftime("%Y-%m-%d %H:%M:%S")}'
        log.info(f'Creating new playlist {playlist_name}')
        playlist = self.request.user.playlist_set.create(name=playlist_name)
        for track in tracks:
            playlist.playlistentry_set.create(
                artist=', '.join(artist.name for artist in track.artists),
                name=track.name,
                uri=track.uri,
                preview_url=track.preview_url,
                cover_art_url=[image for image in track.album.images if image.height == 300][0].url,
            )

        return super().get(request, playlist.pk, *args, **kwargs)

    def get_redirect_url(self, playlist_id, *args, **kwargs):
        return reverse('playlist-detail', args=[playlist_id])
