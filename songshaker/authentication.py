import logging

from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User

import tekore as tk

from songshaker.spotify import SpotifyAuthenticator

log = logging.getLogger(__name__)


class SpotifyBackend(BaseBackend):
    def authenticate(self, request, code, **kwargs):
        user_token = SpotifyAuthenticator().get_user_token_from_code(code)

        spotify = tk.Spotify(token=user_token)
        user_id = spotify.current_user().id

        user, created = User.objects.get_or_create(username=user_id)
        if created:
            log.info(f'Created new user {user.username}')

        if not user.token_set.filter(
            access_token=user_token.access_token, refresh_token=user_token.refresh_token
        ).exists():
            user.token_set.create(access_token=user_token.access_token, refresh_token=user_token.refresh_token)

        return user

    def get_user(self, user_id):
        return User.objects.filter(id=user_id).first()
