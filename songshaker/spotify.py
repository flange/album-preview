import logging
from functools import lru_cache

import tekore as tk
from tekore import scope

log = logging.getLogger(__name__)


class SpotifyClientTokenBase:
    def __init__(self):
        client_id, client_secret, _ = tk.config_from_environment()
        client_token = tk.request_client_token(client_id, client_secret)
        self.spotify = tk.Spotify(client_token)


class SpotifyRefreshTokenBase:
    def __init__(self, refresh_token):
        client_id, client_secret, redirect_uri = tk.config_from_environment()
        cred = tk.Credentials(client_id, client_secret, redirect_uri)
        token = cred.refresh(refresh_token)
        self.spotify = tk.Spotify(token=token)


class SpotifyAlbumFetcher(SpotifyClientTokenBase):
    def get_album_uri(self, artist, album):
        log.info(f'Getting album URI for album {album} by {artist}')
        query = f'album:{album} artist:{artist}'
        results = self.spotify.search(query=query, limit=50, types=('album',))
        if not results[0].items:
            return

        return results[0].items[0].id

    def get_most_popular_tracks(self, album_id, limit):
        log.info(f'Getting most {limit} popular tracks for ID {album_id}')
        album = self.spotify.album(album_id)
        tracks = self.spotify.tracks([track.id for track in album.tracks.items])

        sorted_tracks = sorted(tracks, key=lambda track: track.popularity, reverse=True)
        return sorted_tracks[:limit]


class SpotifyPlaylistCreator(SpotifyRefreshTokenBase):
    def create_spotify_playlist(self, username, name, track_uris):
        playlist_data = self.spotify.playlist_create(username, name=name, public=False)
        playlist_id = playlist_data.id
        log.info(f'Created playlist {playlist_id} for user {username}')

        self.spotify.playlist_add(playlist_id, track_uris)
        log.info(f'Added {len(track_uris)} to playlist {playlist_id}')
        return playlist_data


class SpotifyAuthenticator:
    def __init__(self):
        client_id, client_secret, redirect_uri = tk.config_from_environment()
        self.cred = tk.Credentials(client_id, client_secret, redirect_uri)

    def get_auth_url(self):
        return self.cred.user_authorisation_url(
            scope=f'{scope.user_top_read},'
            f'{scope.playlist_modify_private},'
            f'{scope.playlist_read_collaborative},'
            f'{scope.playlist_read_private}',
            show_dialog=True,
        )

    def get_user_token_from_code(self, code):
        return self.cred.request_user_token(code)


class SpotifyPersonalisationFetcher(SpotifyRefreshTokenBase):
    def current_user_top_artists(self, limit=50, **kwargs):
        return self.spotify.current_user_top_artists(limit=limit, **kwargs).items

    def current_user_get_all_playlists(self, **kwargs):
        return self.spotify.all_items(self.spotify.playlists(self.spotify.current_user().id, **kwargs))

    @lru_cache
    def current_user_get_playlist_tracks(self, playlist_name, **kwargs):
        playlists = self.current_user_get_all_playlists(**kwargs)
        discover_weekly = list(filter(lambda playlist: playlist.name == playlist_name, playlists))[0]
        return self.spotify.playlist(discover_weekly.id).tracks.items


class SpotifyArtistsFetcher(SpotifyClientTokenBase):
    @lru_cache
    def artist_albums(self, artist_id, limit=50):
        return self.spotify.artist_albums(artist_id, limit=limit).items

    @lru_cache
    def top_tracks(self, artist_id, **kwargs):
        return self.spotify.artist_top_tracks(artist_id, **kwargs)
