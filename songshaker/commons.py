import random

from more_itertools import collapse, map_reduce, take


def group_tracks_by_artists_and_select_random_choice(tracks, number_of_tracks, tracks_per_artist=1):
    grouped_tracks = map_reduce(tracks, keyfunc=lambda track: track.artists[0].name, reducefunc=list)
    selected_artists = random.sample(grouped_tracks.keys(), number_of_tracks)
    return collapse(
        take(tracks_per_artist, artist_tracks)
        for artist_tracks in [grouped_tracks[artist] for artist in selected_artists]
    )
