from unittest import mock

from django.test import Client
from django.urls import reverse

from spotipy import SpotifyOAuth


def test_login_view():
    client = Client()

    with mock.patch.object(SpotifyOAuth, 'get_authorize_url', return_value='auth_url') as get_authorize_url_mock:
        response = client.get(reverse('login'))

    assert get_authorize_url_mock.called
    assert response.status_code == 200
    assert response.context_data['auth_url'] == 'auth_url'
