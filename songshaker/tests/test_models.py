from unittest import mock
from unittest.mock import call

from django.contrib.auth.models import User

import pytest
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField
from hypothesis import assume, given, strategies
from hypothesis.extra.django import from_model, register_field_strategy
from hypothesis.extra.pytz import timezones
from hypothesis.strategies import just

from songshaker.models import Playlist
from songshaker.parser import InputParser
from songshaker.spotify import SpotifyAlbumFetcher

register_field_strategy(CreationDateTimeField, strategies.datetimes(timezones=timezones()))
register_field_strategy(ModificationDateTimeField, strategies.datetimes(timezones=timezones()))

playlist_strategy = from_model(Playlist, user=from_model(User))
fixed_input_playlist_strategy = from_model(
    Playlist,
    user=from_model(User),
    input=just('artist - title'),
    finished_processing=just(None),
)

song_response_strategy = strategies.lists(
    strategies.fixed_dictionaries(
        {
            'name': strategies.text(),
            'uri': strategies.text(),
            'artists': strategies.lists(strategies.fixed_dictionaries({'name': strategies.text()}), min_size=1),
        }
    ),
    min_size=1,
)


@pytest.mark.django_db
@given(playlist=playlist_strategy)
def test_playlist_already_processed(playlist):
    assume(playlist.finished_processing)

    with mock.patch.object(InputParser, 'create_albums') as create_album_mock:
        playlist.process()

    assert not create_album_mock.called


@pytest.mark.django_db
@given(playlist=fixed_input_playlist_strategy, most_popular_tracks=song_response_strategy)
def test_playlist_not_processed(playlist, most_popular_tracks):
    assume(playlist.finished_processing is None)

    with mock.patch.object(
        SpotifyAlbumFetcher, 'get_album_uri', return_value='uri'
    ) as get_album_uri_mock, mock.patch.object(
        SpotifyAlbumFetcher, 'get_most_popular_tracks', return_value=most_popular_tracks
    ) as get_most_popular_tracks_mock:
        playlist.process()

    assert get_most_popular_tracks_mock.call_args == call('uri', 2)
    assert playlist.playlistentry_set.count() == len(most_popular_tracks)
    assert get_album_uri_mock.call_args == call('title')

    playlist.refresh_from_db()

    assert playlist.finished_processing is not None
