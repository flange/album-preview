from hypothesis import assume, given, settings
from hypothesis.strategies import characters, text

from songshaker.parser import Album, InputParser


@given(
    artist=text(characters(blacklist_categories=('Cs', 'Z', 'Cc'))),
    name=(text(characters(blacklist_categories=('Cs', 'Z', 'Cc')))),
)
@settings(print_blob=True, deadline=None)
def test_input_parser(artist, name):
    assume(artist)
    assume(name)

    result = InputParser(f'{artist} - {name}').create_albums()

    assert result == [Album(artist=artist, name=name)]
