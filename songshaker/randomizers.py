import random

from more_itertools import take
from more_itertools.more import chunked, collapse

from songshaker.commons import group_tracks_by_artists_and_select_random_choice
from songshaker.spotify import SpotifyArtistsFetcher, SpotifyPersonalisationFetcher


class RandomizerBase:
    def __init__(self, refresh_token):
        self.refresh_token = refresh_token
        self.fetcher = SpotifyPersonalisationFetcher(refresh_token=self.refresh_token)

    def get_tracks(self):
        raise NotImplementedError


class BasedOnTopArtistsRandomizer(RandomizerBase):
    NUMBER_OF_TRACKS_PER_PLAYLIST = 3
    NUMBER_OF_TRACKS_PER_ARTIST = 1
    TIME_RANGE = 'short_term'

    def get_tracks(self):
        top_artists = self.fetcher.current_user_top_artists(time_range=self.TIME_RANGE, limit=10)
        random.shuffle(top_artists)
        track_list = []

        for artist in top_artists:
            top_tracks = SpotifyArtistsFetcher().top_tracks(
                artist.id, market=self.fetcher.spotify.current_user().country or 'DE'
            )
            random_top_track = random.sample(top_tracks, self.NUMBER_OF_TRACKS_PER_ARTIST)
            track_list.extend(random_top_track)

        return track_list


class BasedOnDiscoverWeekly(RandomizerBase):
    NUMBER_OF_TRACKS_PER_ARTIST = 1
    NUMBER_OF_TRACKS_PER_PLAYLIST = 22
    PLAYLIST_NAME = 'Discover Weekly'

    def get_tracks(self):
        tracks = self.fetcher.current_user_get_playlist_tracks(self.PLAYLIST_NAME)
        artist_ids = [track.track.artists[0].id for track in tracks]
        random.shuffle(artist_ids)
        chunked_artist_ids = chunked(artist_ids, 5)
        tracks = list(
            collapse(
                [
                    self.fetcher.spotify.recommendations(artist_ids=artist_ids).tracks
                    for artist_ids in chunked_artist_ids
                ]
            )
        )
        return group_tracks_by_artists_and_select_random_choice(
            tracks, self.NUMBER_OF_TRACKS_PER_PLAYLIST, self.NUMBER_OF_TRACKS_PER_ARTIST
        )


class BasedOnGenresNewReleases(RandomizerBase):
    NUMBER_OF_TRACKS_PER_PLAYLIST = 5
    PLAYLIST_NAME = 'Discover Weekly'

    def get_tracks(self):
        tracks = self.fetcher.current_user_get_playlist_tracks(self.PLAYLIST_NAME)
        artist_ids = [track.track.artists[0].id for track in tracks]
        genre_set = set(collapse([artist.genres for artist in self.fetcher.spotify.artists(artist_ids)]))
        genre_picks = random.sample(genre_set, self.NUMBER_OF_TRACKS_PER_PLAYLIST)
        tracks = collapse([self.fetcher.spotify.search(f'genre:"{genre}"+year:2020')[0].items for genre in genre_picks])
        return group_tracks_by_artists_and_select_random_choice(tracks, self.NUMBER_OF_TRACKS_PER_PLAYLIST)


class TrackCombiner:
    def __init__(self, refresh_token):
        self.refresh_token = refresh_token
        self.sources = [
            BasedOnTopArtistsRandomizer(refresh_token),
            BasedOnDiscoverWeekly(refresh_token),
            BasedOnGenresNewReleases(refresh_token),
        ]

    def get_tracks(self):
        track_list = []
        for source in self.sources:
            tracks = take(source.NUMBER_OF_TRACKS_PER_PLAYLIST, source.get_tracks())
            track_list.extend(tracks)

        return track_list
