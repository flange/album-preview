import logging
import uuid

from django.contrib.auth.models import User
from django.db import models

from django_extensions.db.models import TimeStampedModel

log = logging.getLogger(__name__)


class Token(TimeStampedModel):
    class Meta:
        ordering = ('-created',)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    access_token = models.TextField()
    refresh_token = models.TextField()


class Playlist(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)

    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    submitted_to_spotify = models.DateTimeField(null=True)
    spotify_uri = models.TextField(null=True)


class PlaylistEntry(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)

    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    artist = models.TextField()
    name = models.TextField()
    uri = models.TextField()
    preview_url = models.URLField(null=True)
    cover_art_url = models.URLField(null=True)
