from django.contrib.auth.views import LogoutView
from django.urls import path

from songshaker.views import AuthCallbackView, LoginView, PlayListDetailView, RandomPlaylistView, SubmitPlaylistView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('playlist/<uuid:pk>', PlayListDetailView.as_view(), name='playlist-detail'),
    path('', RandomPlaylistView.as_view()),
    path('random/', RandomPlaylistView.as_view(), name='playlist-random'),
    path('submit/<uuid:pk>', SubmitPlaylistView.as_view(), name='submit-playlist'),
    path('authcallback/', AuthCallbackView.as_view()),
]
