FROM python:3.8.5-buster

RUN groupadd -r songshaker && useradd -m -g songshaker songshaker

RUN mkdir /app
RUN chown songshaker:songshaker /app -R

COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

USER songshaker
WORKDIR /app

COPY --chown=songshaker manage.py setup.cfg /app/
COPY --chown=songshaker songshaker /app/songshaker/
COPY --chown=songshaker templates /app/templates
COPY --chown=songshaker uwsgi.ini /app/uwsgi.ini

EXPOSE 8000
