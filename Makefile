format:
	docker-compose run web autoflake -i -r --remove-all-unused-imports --remove-unused-variables songshaker
	docker-compose run web isort --profile black .
	docker-compose run web black .

test:
	docker-compose run web pytest --cov --cov-report=term:skip-covered
