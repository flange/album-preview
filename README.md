# Songshaker

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Tested with Hypothesis](https://img.shields.io/badge/hypothesis-tested-brightgreen.svg)](https://hypothesis.readthedocs.io/)

### What is this

This purpose of this app is to create Spotify playlists, either using bulk input or random based on the listening
behaviour of the user. To check out the live app visit https://songshaker.com.

### Spotify auth
Currently there are 2 auth flows implemented:
* [Auth code flow](https://developer.spotify.com/documentation/general/guides/authorization-guide/#authorization-code-flow)
* [Client credentials flow](https://developer.spotify.com/documentation/general/guides/authorization-guide/#client-credentials-flow)

### How to run
1. create an `.env` file with all the necessary settings
1. run `docker-compose build && docker-compose up` to launch a dev environment
1. check `localhost:8000` to get started
